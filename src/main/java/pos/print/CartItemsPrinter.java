package pos.print;

import pos.cart.Cart;
import pos.common.Helpers;

import java.math.BigDecimal;

public class CartItemsPrinter {

    Helpers helpers = new Helpers();

    public void print(Cart cart) {

        cart.getLineItems().forEach(lineItem -> {
            System.out.printf("product id: %s, product name: %s, price %s, tax category %s\n", lineItem.getProductId(), lineItem.getProductName(), lineItem.getPrice(), lineItem.getTaxCategory());
        });
        helpers.newLines(2);
    }
}

