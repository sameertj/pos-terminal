package pos.print.receipt;

import pos.cart.Cart;
import pos.common.Helpers;

import java.math.BigDecimal;

public class ReceiptReceiptPrinter implements ReceiptPrinterIfc {

    Helpers helpers = new Helpers();

    @Override
    public void print(Cart cart, BigDecimal payment) {

        BigDecimal total = cart.computeTotal();

        System.out.println("Printing Receipt...");
        System.out.println("product id , product name, price, tax_category , county_tax, state_tax, city_tax");

        cart.getLineItems().forEach(lineItem -> {
            System.out.printf("%s %s %s %s %s %s %s\n", lineItem.getProductId(), lineItem.getProductName(), lineItem.getPrice().toString(), lineItem.getTaxCategory()
                    , lineItem.getCountyTax().toString(), lineItem.getStateTax().toString(), lineItem.getCityTax().toString());
        });

        System.out.printf("Total: %s\n", total.toString());
        System.out.printf("Customer paid:%s\n", payment.toString());
        System.out.printf("Change due: %s\n", payment.subtract(total).toString());
        helpers.newLines(1);
    }


}
