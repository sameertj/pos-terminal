package pos.print.receipt;

import pos.cart.Cart;

import java.math.BigDecimal;

public interface ReceiptPrinterIfc {

    public void print(Cart cart, BigDecimal payment);
}
