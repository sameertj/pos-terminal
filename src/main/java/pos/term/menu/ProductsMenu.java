package pos.term.menu;

import pos.cart.LineItem;
import pos.common.Helpers;
import pos.domain.Product;
import pos.print.CartItemsPrinter;
import pos.term.PosTerminal;

import java.util.*;

public class ProductsMenu {

    private PosTerminal posTerminal;
    Helpers helpers = new Helpers();

    CartItemsPrinter cartItemsPrinter = new CartItemsPrinter();

    public ProductsMenu(PosTerminal posTerminal)
    {
        this.posTerminal = posTerminal;
    }

    public void productLoop() {

        loop:
        while (true) {
            displayProductPageOptions();
            int productPageOption = getProductPageOption();
            switch (productPageOption) {
                case 1:
                    productIdLoop();
                    break;
                case 2:
                    System.out.println("printing cart items...");
                    cartItemsPrinter.print(posTerminal.getCart());
                    break;
                case 3:
                    System.out.println("exiting product page.");
                    break loop;
            }
        }
    }

    public void displayProductPageOptions() {

        List<String> options = new ArrayList<>();
        options.add("ENTER A PRODUCT ID");
        options.add("PRINT CART ITEMS");
        options.add("EXIT");
        System.out.println("Please choose from one of the following options.");
        for (int i = 0; i < options.size(); i++) {
            System.out.printf("%d, %s\n", i + 1, options.get(i));
        }
    }

    public int getProductPageOption() {

        int option = posTerminal.getScanner().nextInt();
        System.out.println("you chose " + option);
        helpers.newLines(1);
        return option;
    }

    public void productIdLoop() {

        System.out.println("Enter a valid product id or a product prefix....");
        String productId = getProductId();
        if (productId == null || productId.equals("") || productId.length() > 12) {
            System.out.println("product id is invalid.");
        } else if (productId.length() == 12 && posTerminal.getProductMap().containsKey(productId)) {
            Product product = posTerminal.getProductMap().get(productId);
            LineItem lineItem = new LineItem(product);
            //lineItems.add(lineItem);
            posTerminal.getCart().addLineItem(lineItem);
            System.out.printf("product %s is added.\n", productId);
            helpers.newLines(1);
        } else if (productId.length() < 12) {
            List<String> productIds = posTerminal.getTrie().searchByPrefix(productId);
            multiProductLoop(productIds);
        }
    }

    private String getProductId() {

        String productId = posTerminal.getScanner().next();
        System.out.println("Entered product id is: " + productId);
        helpers.newLines(1);
        return productId;
    }

    public void multiProductLoop(List<String> productIds) {

        if (productIds.size() == 0) {
            System.out.println("no products were found.");
            helpers.newLines(1);
            return;
        }

        System.out.println("the following products were found.");
        for (String productId : productIds) {
            System.out.println(productId);
        }

        helpers.newLines(1);

        int option = getMultiProductOption();
        if (option == 1) {
            multiProductSelection(productIds);
        } else if (option == 2) {
            System.out.println("ignoring current list...");
            helpers.newLines(1);
        } else if (option > 2) {
            System.out.println("invalid option is selected");
            helpers.newLines(1);
        }
    }

    public void multiProductSelection(List<String> productIds) {

        Map<Integer, String> plist = new HashMap<>();
        for (int i = 0; i < productIds.size(); i++) {
            plist.put(i + 1, productIds.get(i));
        }

        System.out.println("choose numbered index from the following ..");
        for (int index = 0; index < productIds.size(); index++) {
            System.out.printf("%d %s\n", index + 1, productIds.get(index));
        }

        int index = getProductListIndex();
        if (plist.containsKey(index)) {
            String productId = plist.get(index);
            //lineItems.add(new LineItem(productMap.get(productId)));
            posTerminal.getCart().addLineItem(new LineItem(posTerminal.getProductMap().get(productId)));
            System.out.printf("%s is added to cart\n",productId);
            helpers.newLines(1);

        }
    }

    public int getProductListIndex() {

        int index = posTerminal.getScanner().nextInt();
        System.out.println("entered index is: " + index);
        return index;
    }

    public int getMultiProductOption() {
        System.out.println("Enter 1 for choosing the product id from the above list");
        System.out.println("Enter 2 to exit");

        int option = posTerminal.getScanner().nextInt();
        System.out.println("you chose " + option);
        helpers.newLines(1);
        return option;
    }
}
