package pos.term.menu;

public enum MainMenuAction {

    EXIT,
    START_PURCHASE,
    RECEIPT,
    PRINT_OPTIONS,
    EXIT_TRANSACTION,
    INVALID_OPTION;

    public static MainMenuAction fromInteger(int x) {
        switch(x) {
            case 1:
                return EXIT;
            case 2:
                return START_PURCHASE;
            case 3:
                return RECEIPT;
            case 4:
                return PRINT_OPTIONS;
            case 5:
                return EXIT_TRANSACTION;
            default:
                return INVALID_OPTION;
        }
    }
}