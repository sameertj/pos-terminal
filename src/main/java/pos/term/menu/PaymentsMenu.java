package pos.term.menu;

import pos.common.Helpers;
import pos.term.PosTerminal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PaymentsMenu {

    PosTerminal posTerminal;
    Helpers helpers = new Helpers();

    public PaymentsMenu(PosTerminal posTerminal){
        this.posTerminal = posTerminal;
    }

    public void paymentLoop(PosTerminal posTerminal) {

        loop:
        while (true) {
            displayPaymentMenu();
            int productPageOption = getPaymentOption();
            switch (productPageOption) {
                case 1:
                    String paymentStr = getCustomerAmount();
                    posTerminal.setPayment(posTerminal.getPayment().add(new BigDecimal(paymentStr)));
                    break;
                case 2:
                    System.out.println("exiting product page.");
                    helpers.newLines(1);
                    break loop;
                default:
                    System.out.println("Invalid option was selected.");
            }
        }
    }

    public void displayPaymentMenu() {

        List<String> options = new ArrayList<>();
        options.add("ENTER PAYMENT");
        options.add("EXIT");
        System.out.println("Please choose from one of the following options.");
        for (int i = 0; i < options.size(); i++) {
            System.out.printf("%d, %s\n", i + 1, options.get(i));
        }
    }

    private String getCustomerAmount() {
        System.out.println("Enter a payment amount.");
        String payment = posTerminal.getScanner().next();
        System.out.println("Entered payment is: " + payment);
        helpers.newLines(1);
        return payment;
    }

    private int getPaymentOption() {

        int option = posTerminal.getScanner().nextInt();
        System.out.println("you chose " + option);
        helpers.newLines(1);
        return option;
    }
}
