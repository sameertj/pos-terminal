package pos.term.menu;

import pos.common.Helpers;
import pos.term.PosTerminal;

import java.util.ArrayList;
import java.util.List;

public class MainMenu {

    Helpers helpers = new Helpers();
    PosTerminal posTerminal;

    ProductsMenu productsMenu;
    PaymentsMenu paymentsMenu;

    public MainMenu(PosTerminal posTerminal) {
        this.posTerminal = posTerminal;
        productsMenu = new ProductsMenu(posTerminal);
        paymentsMenu = new PaymentsMenu(posTerminal);
    }

    public void printMenuOptions(PosTerminal posTerminal) {

        List<String> options = new ArrayList<>();
        MainMenuAction[] main_menu_choices = MainMenuAction.values();
        System.out.println("Please choose from following menu options.");
        helpers.newLines(1);
        for (int i = 0; i < main_menu_choices.length - 1; i++) {
            System.out.printf("%d. %s\n", i + 1, main_menu_choices[i].toString());
        }
        helpers.newLines(1);
    }

    public int getMenuAction() {

        int action = posTerminal.getScanner().nextInt();
        System.out.println("you chose " + action);
        helpers.newLines(1);
        return action;
    }

    public void menuLoop(MainMenuAction MenuChoice, PosTerminal posTerminal) {
        switch (MenuChoice) {
            case PRINT_OPTIONS:
                printMenuOptions(posTerminal);
                break;
            case START_PURCHASE:
                if (posTerminal.isInTransaction()) {
                    System.out.println("Already in a transaction. Going to products page.");
                } else {
                    posTerminal.setToInitState();
                    posTerminal.setInTransaction(true);
                }
                productsMenu.productLoop();
                break;
            case RECEIPT:
                if (!posTerminal.isInTransaction()) {
                    System.out.println("Not in a transaction");
                    helpers.newLines(1);
                } else {
                    posTerminal.getTaxCalculator().computeTax(posTerminal.getCart());
                    posTerminal.getReceiptPrinterIfc().print(posTerminal.getCart(), posTerminal.getPayment());
                    paymentsMenu.paymentLoop(posTerminal);
                    posTerminal.getReceiptPrinterIfc().print(posTerminal.getCart(), posTerminal.getPayment());
                }
                break;
            case EXIT_TRANSACTION:
                if (!posTerminal.isInTransaction()) {
                    System.out.println("Not in a transaction.");
                } else {
                    System.out.println("Exiting transaction...");
                    posTerminal.setToInitState();
                }
                helpers.newLines(1);
        }
    }

    public void start() {
        printMenuOptions(posTerminal);
        int choice = getMenuAction();
        MainMenuAction main_menu_action = MainMenuAction.fromInteger(choice);
        while (main_menu_action != MainMenuAction.EXIT) {
            menuLoop(main_menu_action, posTerminal);
            printMenuOptions(posTerminal);
            main_menu_action = MainMenuAction.fromInteger(getMenuAction());
        }
        exit();
    }

    private void exit() {
        System.out.println("bye.");
    }
}
