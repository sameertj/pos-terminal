package pos.term;

import pos.cart.Cart;
import pos.common.Helpers;
import pos.data.Trie;
import pos.domain.Product;
import pos.print.CartItemsPrinter;
import pos.print.ConsolePrinter;
import pos.print.receipt.ReceiptReceiptPrinter;
import pos.tax.TaxCalculator;
import pos.term.menu.MainMenu;

import java.math.BigDecimal;
import java.util.*;

import static java.util.stream.Collectors.toMap;

public class PosTerminal {

    Map<String, Product> productMap;
    Trie trie;
    Scanner scanner = new Scanner(System.in);
    boolean inTransaction = false;
    BigDecimal payment = new BigDecimal(0);

    Helpers helpers = new Helpers();

    MainMenu mainMenu;

    Cart cart = new Cart();
    TaxCalculator taxCalculator = new TaxCalculator();
    ReceiptReceiptPrinter receiptPrinterIfc = new ReceiptReceiptPrinter();
    ConsolePrinter consolePrinter = new ConsolePrinter();

    public PosTerminal(Trie trie, Map<String, Product> productMap) {
        this.trie = trie;
        this.productMap = productMap;
        mainMenu = new MainMenu(this);
    }

    public void start() {
        mainMenu.start();
    }

    public void setToInitState() {

        inTransaction = false;
        cart.clear();
        payment = new BigDecimal(0);
        consolePrinter.print("System is ready for new purchase transaction.");
        helpers.newLines(1);
    }

    public Map<String, Product> getProductMap() {
        return productMap;
    }

    public Trie getTrie() {
        return trie;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public boolean isInTransaction() {
        return inTransaction;
    }

    public void setInTransaction(boolean inTransaction) {
        this.inTransaction = inTransaction;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }

    public Cart getCart() {
        return cart;
    }

    public TaxCalculator getTaxCalculator() {
        return taxCalculator;
    }

    public ReceiptReceiptPrinter getReceiptPrinterIfc() {
        return receiptPrinterIfc;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
}

