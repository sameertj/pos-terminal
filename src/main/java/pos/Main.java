package pos;

import pos.data.Trie;
import pos.domain.Product;
import pos.term.PosTerminal;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        String fileName = "./src/main/java/product-data.txt";
        Trie trie = new Trie();
        Map<String,Product> productMap = new HashMap<>();
        trie.fileRead(fileName);
        File file = new File(fileName);
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] fields = line.split(",");
            String productId = fields[0];
            String productName = fields[1];
            BigDecimal price = new BigDecimal(fields[2]);
            String taxCategory = fields[3];
            Product p = new Product();
            p.setProductName(productName);
            p.setProductId(productId);
            p.setPrice(price);
            p.setTaxCategory(taxCategory);
            productMap.put(productId, p);
        }

        PosTerminal posTerminal = new PosTerminal(trie,productMap);
        posTerminal.start();
    }

}
