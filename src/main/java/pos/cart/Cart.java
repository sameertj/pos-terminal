package pos.cart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Cart {

    List<LineItem> lineItems = new ArrayList<>();

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void clear(){
        lineItems.clear();
    }

    public void addLineItem(LineItem lineItem){
        lineItems.add(lineItem);
    }

    public BigDecimal computeTotal() {

        BigDecimal total = new BigDecimal(0);

        for (LineItem lineItem : lineItems) {
            BigDecimal initial = new BigDecimal(0);
            BigDecimal lineItemTotal = initial.add(lineItem.getPrice()).add(lineItem.getStateTax()).add(lineItem.getCityTax()).add(lineItem.getCountyTax());
            total = total.add(lineItemTotal);
        }

        return total;

    }
}
