package pos.cart;

import pos.domain.Product;

import java.math.BigDecimal;

public class LineItem {

    private String productName;
    private String productId;
    private BigDecimal price;
    private String taxCategory;
    private BigDecimal countyTax = new BigDecimal(0);
    private BigDecimal cityTax = new BigDecimal(0);
    private BigDecimal stateTax = new BigDecimal(0);


    public LineItem(Product product) {
        this.productName = product.getProductName();
        this.productId = product.getProductId();
        this.price = product.getPrice();
        this.taxCategory = product.getTaxCategory();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTaxCategory() {
        return taxCategory;
    }

    public void setTaxCategory(String taxCategory) {
        this.taxCategory = taxCategory;
    }

    public BigDecimal getCountyTax() {
        return countyTax;
    }

    public BigDecimal getCityTax() {
        return cityTax;
    }

    public BigDecimal getStateTax() {
        return stateTax;
    }

    public void setCountyTax(BigDecimal countyTax) {
        this.countyTax = countyTax;
    }

    public void setCityTax(BigDecimal cityTax) {
        this.cityTax = cityTax;
    }

    public void setStateTax(BigDecimal stateTax) {
        this.stateTax = stateTax;
    }
}
