package pos.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class StateTaxCalculator implements TaxCalculatorIfc {


    @Override
    public BigDecimal calculateTax(BigDecimal price) {
        BigDecimal oneHundred = new BigDecimal(100);
        BigDecimal stateTaxPercent = new BigDecimal("6.3").divide(oneHundred);
        return price.multiply(stateTaxPercent).setScale(2, RoundingMode.FLOOR);
    }
}
