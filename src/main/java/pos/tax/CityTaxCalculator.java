package pos.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CityTaxCalculator implements TaxCalculatorIfc{
    @Override
    public BigDecimal calculateTax(BigDecimal price) {
        BigDecimal oneHundred = new BigDecimal(100);
        BigDecimal cityTaxPercent = new BigDecimal("2").divide(oneHundred);
        return price.multiply(cityTaxPercent).setScale(2, RoundingMode.FLOOR);
    }
}
