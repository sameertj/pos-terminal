package pos.tax;

import pos.cart.Cart;
import pos.cart.LineItem;

import java.math.BigDecimal;

public class TaxCalculator{

    CityTaxCalculator cityTaxCalculator = new CityTaxCalculator();
    CountyTaxCalculator countyTaxCalculator = new CountyTaxCalculator();
    StateTaxCalculator stateTaxCalculator = new StateTaxCalculator();

    public void computeTax(Cart cart) {
        cart.getLineItems().forEach(this::lineItemTax);
    }

    private void lineItemTax(LineItem lineItem) {
        BigDecimal cityTax = cityTaxCalculator.calculateTax(lineItem.getPrice());
        lineItem.setCityTax(cityTax);
        if (!lineItem.getTaxCategory().equals("g")) {
            BigDecimal countyTax = countyTaxCalculator.calculateTax(lineItem.getPrice());
            lineItem.setCountyTax(countyTax);
            BigDecimal stateTax = stateTaxCalculator.calculateTax(lineItem.getPrice());
            lineItem.setStateTax(stateTax);
        }
    }
}
