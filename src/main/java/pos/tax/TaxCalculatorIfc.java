package pos.tax;

import java.math.BigDecimal;

public interface TaxCalculatorIfc {

    public BigDecimal calculateTax(BigDecimal price);

}
