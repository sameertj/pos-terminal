package pos.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CountyTaxCalculator implements TaxCalculatorIfc{
    @Override
    public BigDecimal calculateTax(BigDecimal price) {
        BigDecimal oneHundred = new BigDecimal(100);
        BigDecimal countyTaxPercent = new BigDecimal("0.7").divide(oneHundred);
        return price.multiply(countyTaxPercent).setScale(2, RoundingMode.FLOOR);
    }
}
