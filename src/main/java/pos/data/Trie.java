package pos.data;

import pos.data.TrieNode;

import java.io.File;
import java.util.*;

public class Trie {

    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public void fileRead(String fileName) throws Exception {
        File file = new File(fileName);
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] fields = line.split(",");
            String productId = fields[0];
            this.insert(productId);
        }
    }

    public void insert(String word) {
        TrieNode current = root;

        for (char l : word.toCharArray()) {
            current = current.getChildren().computeIfAbsent(l, c -> new TrieNode());
        }
        current.setEndOfWord(true);
    }

    boolean delete(String word) {
        return delete(root, word, 0);
    }

    public boolean containsNode(String word) {
        TrieNode current = root;

        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            TrieNode node = current.getChildren().get(ch);
            if (node == null) {
                return false;
            }
            current = node;
        }
        return current.isEndOfWord();
    }

    public TrieNode returnNode(String word) {
        TrieNode current = root;

        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            TrieNode node = current.getChildren().get(ch);
            if (node == null) {
                return null;
            }
            current = node;
        }
        return current;
    }

    public List<String> searchByPrefix(String prefix) {
        return searchByPrefix(this.getRoot(),prefix);
    }

    public List<String> searchByPrefix(TrieNode tn, String prefix) {
        TrieNode n = returnNode(prefix);
        List<String> productIds = new ArrayList<>();
        if (n != null) {
            depthSearch(n, prefix, productIds);
        }
        return productIds;
    }

    public void depthSearch(TrieNode tn, String prefix, List<String> lst) {
        if (tn.isEndOfWord()) {
            lst.add(prefix);
        }

        Iterator<Map.Entry<Character, TrieNode>> entryIterator = tn.getChildren().entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<Character, TrieNode> entry = entryIterator.next();
            depthSearch(entry.getValue(), prefix + entry.getKey(), lst);
        }
    }
    
    boolean isEmpty() {
        return root == null;
    }

    private boolean delete(TrieNode current, String word, int index) {
        if (index == word.length()) {
            if (!current.isEndOfWord()) {
                return false;
            }
            current.setEndOfWord(false);
            return current.getChildren().isEmpty();
        }
        char ch = word.charAt(index);
        TrieNode node = current.getChildren().get(ch);
        if (node == null) {
            return false;
        }
        boolean shouldDeleteCurrentNode = delete(node, word, index + 1) && !node.isEndOfWord();

        if (shouldDeleteCurrentNode) {
            current.getChildren().remove(ch);
            return current.getChildren().isEmpty();
        }
        return false;
    }

    public TrieNode getRoot() {
        return root;
    }
}